package br.edu.fatecsjc;

public interface Pilha {

    public final static int MAX_CARTAS = 52;
    public void empilhar(Carta carta);
    public Carta desempilhar();


}
