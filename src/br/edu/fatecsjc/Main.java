package br.edu.fatecsjc;

public class Main {

    public static void main(String[] args) {

        // Pilha LIFO
        // Fila FIFO

        System.out.println("Cria��o do Projeto da Atividade: Baralho");

        Baralho baralho = new Baralho();
        // System.out.println("Tamanho baralho: " + baralho.getSize());
        System.out.println("Tamanho do baralho: ".concat(String.valueOf(baralho.getSize())));
        System.out.println(baralho.hasCarta());

        baralho.embaralhar();

        for (int i = 1; i <= baralho.getSize(); i++){
            System.out.printf("%-19s", baralho.distribuirCarta());

            if (i % 4 == 0)
                System.out.println();
        }
    }
}
