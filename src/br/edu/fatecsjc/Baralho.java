package br.edu.fatecsjc;

import java.util.Random;

public class Baralho{

    private Carta[] baralho;
    private int cartaAtual;
    private int ultimo;
    private static final int NUMERO_CARTAS = 56;
    private static Random randomCards = new Random();

    public Baralho() {
        String[] nomes = {"As", "Dois", "Tr�s", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Valete", "Dama", "Rei", "Coringa", "Coringa", "Coringa", "Coringa"};
        Naipe[] naipes = {Naipe.CLUBS, Naipe.DIAMONDS, Naipe.HEARTS, Naipe.SPADES};
        baralho = new Carta[NUMERO_CARTAS];

        cartaAtual = 0;
        ultimo = 55;

        for (int posicao = 0; posicao < getSize(); posicao++) {
            baralho[posicao] = new Carta(nomes[posicao % 14], naipes[posicao / 14]);
        }
    }

    public Carta[] getBaralho() {
        return baralho;
    }

    public void setCartas(Carta[] baralho) {
        this.baralho = baralho;
    }

    public int getSize() {
        return baralho.length;
    }

    public void embaralhar() {
        cartaAtual = 0;

        for (int p = 0; p < getSize(); p++) {
            int prox = randomCards.nextInt(NUMERO_CARTAS);

            Carta aux = baralho[p];
            baralho[p] = baralho[prox];
            baralho[prox] = aux;
        }
    }

    /*
    public Carta distribuirCarta() {

        return cartaAtual < getSize() ? baralho[cartaAtual++] : null;
    }

     */


    public Carta distribuirCarta() {

        return ultimo >= 0 ? baralho[ultimo--] : null;
    }


    public boolean hasCarta() {
        System.out.printf("Size %d\n", getSize());

        return getSize() >= 1;
    }
}
