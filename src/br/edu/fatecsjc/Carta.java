package br.edu.fatecsjc;

public class Carta {

    private String nome;
    private Integer naipe;

    public Carta(String nome){
        this.nome = nome;
    }

    public Carta(String nome, Naipe naipe) {
        this.nome = nome;
        this.naipe = (naipe == null) ? null : naipe.getCodigo();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Naipe getNaipe() {
        return Naipe.toEnum(naipe);
    }

    public void setNaipe(Naipe naipe) {
        this.naipe = naipe.getCodigo();
    }

    @Override
    public String toString() {

        return getNome().concat(" de ").concat(getNaipe().getNaipeDescricao());

        // ou utilizando StringBuilder
        /*
        StringBuilder sb = new StringBuilder();
        sb.append(getNome()).append(" de ").append(getNaipe().getNaipeDescricao());
        return sb.toString();
         */
    }
}
