package br.edu.fatecsjc;

import com.sun.xml.internal.bind.v2.model.core.NonElement;

public enum Naipe {

    CLUBS(1, "Paus"),
    DIAMONDS(2, "Ouros"),
    HEARTS(3, "Copas"),
    SPADES(4, "Espadas");

    private int codigo;
    private String naipeDescricao;

    Naipe(int codigo, String naipeDescricao) {
        this.codigo = codigo;
        this.naipeDescricao = naipeDescricao;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNaipeDescricao() {
        return naipeDescricao;
    }

    public static Naipe toEnum(Integer codigo) {

        if (codigo == null)
            return null;

        for (Naipe x : Naipe.values())
            if (codigo.equals(x.getCodigo()))
                return x;
        throw new IllegalArgumentException("Id inv�lido. " + codigo);
    }

}
